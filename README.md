# Example project for shared flows in Mule 4

This is an example project for using shared flows in Mule 4.

Before importing the `myapi` project in Anypoint Studio you i
need to build and install the `commons` project in your local maven 
repository.

```
cd commons
mvn clean install
```

See this Knowledge Article for more details.
https://help.mulesoft.com/s/article/How-to-add-a-call-to-an-external-flow-in-Mule-4
