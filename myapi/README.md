  ## Mule 4 API Template (for REST Based API)

  This is a standalone API template to be used by any Mule project. This API Template can be used as a foundation for any `REST` based API.

The template takes care most of the aspect of API creation

* Logging
*  Error Handling
*  APIKit Router
*  Environment Based Property
*  API Auto-Discovery
*  E2E Transaction tracking through unique `requestId`

## Note:

After project creation you will have to import your APIs RAML specification and rename `.gitlab-ci.template` to `.gitlab-ci.yml`. Also make sure that the `DEPLOY_TARGET` is set to `RTF` or `CH` depending on where the application should be deployed.

You will also have to update:

* `pom.xml`
  * `artifactId` and `name` reflecting the what API the application implements.

*  `src/main/resources/config.properties`
  * The API ID for the API instance 
  * The Application name `application.name`
  * Relative path to your APIs main RAML file
  * The Anypoint Platform client id and secret for the Development environment
  * Update the MUnit test suite and examples in the `src/test/resources/scaffolder` folder to match your API definition

### Logging

There are two type of logging covered by the template -

+ Audit Logging: The Template automatically takes care of logging at the start and end of any transaction irrespective of the transaction status (Failure or Success). Below is sample Audit Log -
```
  INFO  2018-04-11 16:10:38,800 [[MuleRuntime].cpuLight.10: [mule4-api-template].org-s-order-api-main-flow.CPU_LITE @360dc3bf] [event: 0-7d5c50a0-3d9a-11e8-a0db-0a0027000000] org.mule.runtime.core.internal.processor.LoggerMessageProcessor: Audit: {
    "transactionId": "9f7ca5e78a4a45368ae562a6f789ddfc",
    "clientId": null,
    "application": null,
    "dateTimeStamp": "2018-04-11T16:10:38",
    "uri": "/api/org-s-order/v1/orders",
    "httpMethod": "GET",
    "schem": "http",
    "processingStage": "BEGIN",
    "transactionStatus": "Init",
    "httpStatusCode": 200
  }
  INFO  2018-04-11 16:10:38,910 [[MuleRuntime].cpuLight.16: [mule4-api-template].get:\orders:org-s-order-api-router-config.CPU_LITE @116674e5] [event: 0-7d5c50a0-3d9a-11e8-a0db-0a0027000000] org.mule.runtime.core.internal.processor.LoggerMessageProcessor: Audit: {
    "transactionId": "9f7ca5e78a4a45368ae562a6f789ddfc",
    "clientId": null,
    "application": null,
    "message": "Write any message to log",
    "dateTimeStamp": "2018-04-11T16:10:38",
    "uri": "/api/org-s-order/v1/orders",
    "httpMethod": "GET",
    "schem": "http",
    "processingStage": "InProgress",
    "transactionStatus": "InProgress",
    "httpStatusCode": "200"
  }
  INFO  2018-04-11 16:10:39,048 [[MuleRuntime].cpuLight.10: [mule4-api-template].org-s-order-api-main-flow.CPU_LITE @360dc3bf] [event: 0-7d5c50a0-3d9a-11e8-a0db-0a0027000000] org.mule.runtime.core.internal.processor.LoggerMessageProcessor: Audit: {
    "transactionId": "9f7ca5e78a4a45368ae562a6f789ddfc",
    "clientId": null,
    "application": null,
    "message": "Write any message to log",
    "dateTimeStamp": "2018-04-11T16:10:39",
    "uri": "/api/org-s-order/v1/orders",
    "httpMethod": "GET",
    "schem": "http",
    "processingStage": "COMPLTED",
    "transactionStatus": "SUCCESS",
    "httpStatusCode": 200
  }
```

  + AdHoc Logging: The template enforce a standard logging practice. To log any message, developer needs to make use of the `mule-common-logger-subflow` flow. Developer can influence the logging using following variables
  + logMessage: Set this variable with the message you want to print.
  + logCategory: A marker category to be used by logger. Default is `Info`
  + logLevel: The` Slf4j` Log level to be used here. Default is `INFO`. Below is an example -

  ```xml
  <ee:transform doc:name="Logging Config" doc:id="cf4f2fee-2a2c-4ca0-847d-f76c12ad13aa" >
    <ee:message >
    </ee:message>
    <ee:variables >
      <ee:set-variable variableName="logMessage" ><![CDATA[%dw 2.0
  output application/java
  ---
  'Write any message to log']]></ee:set-variable>
      <ee:set-variable variableName="logCategory" ><![CDATA[%dw 2.0
  output application/java
  ---
  'Audit']]></ee:set-variable>
      <ee:set-variable variableName="logLevel" ><![CDATA[%dw 2.0
  output application/java
  ---
  'INFO']]></ee:set-variable>
    </ee:variables>
  </ee:transform>
  <flow-ref doc:name="mule-common-logger-subflow" doc:id="52e03c90-2882-4471-84a1-7a5cde33e540" name="mule-common-logger-subflow"/>
  ```
### Error Handling

Most of the main error types (Like `CONNECTIVITY`, `TRANSFORMATION`, `SCRIPTING`, `EXPRESSION`, `ROUTING`, `SECURITY`, and `ANY`) and some specific error types (`APIKIT:<>``, `HTTP:<>``, and `DB:<>``) are handled in the template. In most of the cases, you do not need to handle any more generic error types, but in case you have a requirement to implement separate behaviour for certain error type then feel free to extend it. You can find the full implementation at error-handling.xml

### APIKit Router

The template make use of the APIKit router. Just use it as it is.


###  API Auto-Discovery

  API Discovery mechanism has changed a lot in Mule 4. Following are the main steps to configure API Auto Discovery in Mule 4:

* While creating an `API Version` in `API Manager`, make sure to click the Mule 4 checkbox, caption as `Check this box if you are managing this API in Mule 4 or above.`.

* In studio, the configuration will look like below
```xml
<api-gateway:autodiscovery apiId="${api.id}" doc:name="API Autodiscovery"  doc:id="c4d74c1e-b320-4b22-8ce6-dc511e54da5b" flowRef="org-s-order-api-main-flow" >
```

* Also, make sure to pass the CloudHub environment's client_id and client_secret as system property to the JVM. This can be using any of the following methods, depending on where you are deploying:
  - CloudHub: If you are deploying to `CloudHub`, please pass them as property in the CloudHub Deployment UI's Property view

```
  anypoint.platform.analytics_base_uri=https://analytics-ingest.anypoint.mulesoft.com
  anypoint.platform.client_id=xxxxxxx
  anypoint.platform.coreservice_base_uri=https://anypoint.mulesoft.com/accounts
  anypoint.platform.client_secret=xxxxx
  anypoint.platform.platform_base_uri=https://anypoint.mulesoft.com/apiplatform
  mule.env=Development
  anypoint.platform.contracts_base_uri=https://anypoint.mulesoft.com/apigateway/ccs
```

  - If you are using Mule Standalone Runtime, you can either configure them in wrapper.conf or pass them as JVM arguments while starting the Mule EE runtime

```
./mule -M-XX:-UseBiasedLocking -M-Dmule.env=dev -M-Danypoint.platform.analytics_base_uri=https://analytics-ingest.anypoint.mulesoft.com -M-Danypoint.platform.client_id=xxx -M-Danypoint.platform.coreservice_base_uri=https://anypoint.mulesoft.com/accounts -M-Danypoint.platform.client_secret=xxx -M-Danypoint.platform.platform_base_uri=https://anypoint.mulesoft.com/apiplatform -M-Danypoint.platform.contracts_base_uri=https://anypoint.mulesoft.com/apigateway/ccs
```
  - Finally, while working in Studio, you can configure the same by 'VM Arguments' section with the same values

```
-M-XX:-UseBiasedLocking -M-Dmule.env=dev -M-Danypoint.platform.analytics_base_uri=https://analytics-ingest.anypoint.mulesoft.com -M-Danypoint.platform.client_id=xxxx -M-Danypoint.platform.coreservice_base_uri=https://anypoint.mulesoft.com/accounts -M-Danypoint.platform.client_secret=xxxxx -M-Danypoint.platform.platform_base_uri=https://anypoint.mulesoft.com/apiplatform -M-Danypoint.platform.contracts_base_uri=https://anypoint.mulesoft.com/apigateway/ccs
```

The code has already been configured and a property has been placed as `api.id` which needs to be populated with the correct API Id from the API Manager.


###  E2E Transaction tracking

The template has a mechanism to either use an existing `requestId` (if present as a header param called `request-id`) or create a new unique request id  (as `UUID`) track the transaction E2E.

If the API making an outbound call, the `vars.requestId` should be passed along otherwise the it won't be useful.
